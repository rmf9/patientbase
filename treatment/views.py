from django.shortcuts import render
from django.urls import reverse
import time
import random

from .models import Personal, Cabinet, Aparat, Procedura, Pacient, Program

from .create_fisa import programPacient, remove, getTotal4SelectedProc, checkForAvailableSpot, getFinishTime

def index(request):
    proceduri = Procedura.objects.order_by('nume')
    return render(request, 'treatment/index.html', {'proceduri': proceduri})

def pacient(request, pacient_id):
    selectii = request.POST.get('mytext', False)
    if selectii:
        pac_id = int(str(random.randint(10000,99999))+str(int(time.time())))
        existaPacient = Pacient.objects.filter(selectii=selectii).filter(activ=True)
    else:
        existaPacient = []
        existaPacient.append(Pacient.objects.get(pacient_id=pacient_id))
    if len(existaPacient) > 0 :
        text = []
        good = []
        pacient = existaPacient[0]
        tempgood = pacient.good.split('||')
        for tmp in tempgood:
            if len(tmp) == 0:
                continue
            good.append(list(filter(None, tmp.split('[['))))
        bad = list(filter(None, pacient.bad.split('||')))
    else:
        text = programPacient(selectii, pacient_id, pac_id)
        pacient = Pacient.objects.get(pacient_id=pacient_id)
        goodtext = ""
        badtext = ""
        good = []
        bad = []
        for txt in text:
            if "could not save" in txt:
                txtparts = txt[15:].replace(":"," - ")
                txtparts = txtparts + " minute"
                bad.append(txtparts)
                badtext = badtext + "||" + txtparts
            else:
                txtparts = txt.split(':')
                newtxt = []
                if len(txtparts[1]) == 3:
                    newtxt.append(txtparts[0][:2] + ":" + txtparts[0][2:] + "-" + "0" + txtparts[1][:1] + ":" + txtparts[1][1:])
                    goodpart = "[["+txtparts[0][:2] + ":" + txtparts[0][2:] + "-" + "0" + txtparts[1][:1] + ":" + txtparts[1][1:]+"[["
                else:
                    newtxt.append(txtparts[0][:2] + ":" + txtparts[0][2:] + "-" + txtparts[1][:2] + ":" + txtparts[1][2:])
                    goodpart = "[["+txtparts[0][:2] + ":" + txtparts[0][2:] + "-" + txtparts[1][:2] + ":" + txtparts[1][2:]+"[["
                goodpart = goodpart + txtparts[2]+"[[" + txtparts[3]+"[[" + txtparts[4]+"[["
                newtxt.append(txtparts[2])
                newtxt.append(txtparts[3])
                newtxt.append(txtparts[4])
                goodtext = goodtext + "||" + goodpart
                good.append(newtxt)
                print(newtxt)
        pacient.good = goodtext + "||"
        pacient.bad = badtext + "||"
        pacient.save()
    return render(request, 'treatment/pacient.html', {'good': good, 'bad': bad, 'pacient': pacient})

def calendar(request):
    cabinete = Cabinet.objects.order_by('nume')
    programari = []
    for cabinet in cabinete:
        programari_cabinet = Program.objects.filter(cabinet=cabinet).order_by('ora')
        rows = []
        for programare in programari_cabinet:
            row = []
            row.append(cabinet.nume)
            if len(programare.ora) == 3:
                row.append("0"+programare.ora[:1]+":"+programare.ora[1:])
            else:
                row.append(programare.ora[:2]+":"+programare.ora[2:])
            id_programati = programare.pacienti.split('-')
            nume_pacienti = []
            for id in id_programati:
                if len(id) == 0 :
                    continue
                nume = Pacient.objects.get(pacient_id=str(id))
                nume_pacienti.append(nume.nume)
            row.append(nume_pacienti)
            rows.append(row)
        programari.append(rows)
    context = {'cabinete': cabinete, 'programari': programari}
    return render(request, 'treatment/calendar.html', context)

def pacienti(request):
    pacienti = Pacient.objects.filter(activ=True).order_by('nume')
    context = {'pacienti': pacienti}
    return render(request, 'treatment/lista_pacienti.html', context)

def deleted(request,pacient):
    #sterge pacient
    om = remove(pacient)
    context = {'pacient': om.nume}
    return render(request, 'treatment/pacient_sters.html', context)

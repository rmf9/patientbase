from django.urls import path
from django.conf.urls import url
from django.contrib import admin
from . import views

app_name = 'treatment'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:pacient_id>/', views.pacient, name='pacient'),
    path('calendar/', views.calendar, name='calendar'),
    path('pacienti/', views.pacienti, name='pacienti'),
    path('deleted/<int:pacient>', views.deleted, name='deleted'),
]
# Generated by Django 2.0.3 on 2018-04-06 06:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('treatment', '0009_auto_20180405_2003'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='data',
            name='aparat',
        ),
        migrations.DeleteModel(
            name='Data',
        ),
    ]

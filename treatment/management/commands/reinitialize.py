from django.core.management.base import BaseCommand
from treatment.models import Program, Cabinet, Pacient

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def _create_tags(self):
        pacienti = Pacient.objects.all()
        for pacient in pacienti:
            print("-----------------------------------------------pentru: "+str(pacient.nume))
            proceduri = Program.objects.filter(pacienti__contains=pacient.pacient_id)
            nume = str(pacient.pacient_id) + "-"
            for procedura in proceduri:
                print(procedura)
                procedura.locuri +=1
                pacienti_moment = procedura.pacienti        
                procedura.pacienti = pacienti_moment.replace(nume,'',1)
                procedura.save()
            pacient.activ = False
            pacient.save()
    def handle(self, *args, **options):
        self._create_tags()
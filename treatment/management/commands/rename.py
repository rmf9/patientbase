from django.core.management.base import BaseCommand
from treatment.models import Program, Cabinet, Pacient

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def _create_tags(self):
        pacienti = Pacient.objects.all()
        i = 0
        for pacient in pacienti:
            print("-----------------------------------------------pentru: "+str(pacient.nume))
            pacient.nume = "pacientul"+str(i)
            i = i + 1
            pacient.save()
    def handle(self, *args, **options):
        self._create_tags()
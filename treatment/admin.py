from django.contrib import admin

from .models import Personal, Cabinet, Aparat, Procedura, Pacient, Program

admin.site.register(Personal)
admin.site.register(Cabinet)
admin.site.register(Aparat)
admin.site.register(Procedura)
admin.site.register(Pacient)
admin.site.register(Program)
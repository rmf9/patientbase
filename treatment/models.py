from django.utils.translation import gettext as _
from django.db import models
from django.utils import timezone
import datetime

class Personal(models.Model):
    tip = models.CharField(max_length=50)
    nr = models.IntegerField(default=1)
    def __str__(self):
        return self.tip

class Cabinet(models.Model):
    personal = models.ForeignKey(Personal, on_delete=models.CASCADE)
    nume = models.CharField(max_length=50)
    paturi = models.IntegerField(default=1)
    def __str__(self):
        return self.nume

class Aparat(models.Model):
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE)
    nume = models.CharField(max_length=50)
    nr = models.IntegerField(default=1)
    max_paturi = models.IntegerField(default=1)
    max_regiuni = models.IntegerField(default=1)
    pregatire_pacient = models.IntegerField(default=5)
    def __str__(self):
        return self.nume

class Procedura(models.Model):
    aparat = models.ForeignKey(Aparat, on_delete=models.CASCADE)
    nume = models.CharField(max_length=50)
    asistent_prezent = models.BooleanField(default=False)
    timpi = models.TextField(default='5')
    regiuni = models.TextField(default='tot')
    prioritate = models.IntegerField(default=0)
    def __str__(self):
        return self.nume
    def lista_regiuni(self):
        if self.regiuni != "tot":
            return sorted(self.regiuni.split(','))
    def lista_timpi(self):
        listaT = self.timpi.split(',')
        if len(listaT) > 1:
            listaInt = []
            for timp in listaT:
                listaInt.append(int(timp))
            return sorted(listaInt, reverse=True)

class Pacient(models.Model):
    pacient_id = models.IntegerField()
    pac_id = models.IntegerField(default=0)
    nume = models.CharField(max_length=50, default="")
    activ = models.BooleanField(default=True)
    aparate = models.TextField(default='-')
    intervale_ocupate = models.TextField(default='-')
    ambulator = models.BooleanField(default=False)
    selectii = models.TextField(default='-')
    good = models.TextField(default='-')
    bad = models.TextField(default='-')
    date_added = models.DateField(_("Date"), default=datetime.date.today)
    def __str__(self):
        return self.nume
    def fisa_electronica(self):
        proceduri = Procedura.objects.all()
        #remove unselected procedures from selections
        details = []
        selections = self.selectii
        for procedura in proceduri:
            firstIndex = selections.find(procedura.nume)
            lastIndex = selections.rfind(procedura.nume)
            if firstIndex != -1 and firstIndex == lastIndex:
                timeString = procedura.nume + ":ztimp"
                restOfString = selections[firstIndex:].find("||")
                if selections.startswith(timeString,firstIndex):
                    if restOfString == -1:
                        selections = selections[:firstIndex]
                    else:
                        actualIndex = firstIndex + restOfString + 2
                        selections = selections[:firstIndex]+selections[actualIndex:]
        lista_selectii = selections.split('||')
        selectii_filtrate = []
        for selectie in lista_selectii:
            if "ztimp" in selectie:
                selectie = selectie.replace("ztimp:","") + "\'"
            selectie = selectie.replace(":"," ")
            selectii_filtrate.append(selectie)
        return selectii_filtrate[2:-1]

class Program(models.Model):
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE)
    ora = models.CharField(max_length=5)
    locuri = models.IntegerField()
    pacienti = models.TextField(default='')
    def __str__(self):
        return self.cabinet.nume + ":ora " + str(self.ora) + ":liber "+str(self.locuri)
function toggleNav() {
	if (document.getElementById("mySidenav").style.width == "100%"){
		document.getElementById("mySidenav").style.width = "0";
	}else {
		document.getElementById("mySidenav").style.width = "100%";
	}
}

function dropDown() {
    document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

function toggleCabinet(cabinetId) {
    var selectedCabinet = document.getElementById(cabinetId);
	var allCabinets = document.getElementsByClassName("cabinet-content");
	for (i = 0; i < allCabinets.length; i++) {
		allCabinets[i].style.display = "none";
	}
    selectedCabinet.style.display = "block";
}

$(document).ready(function () {
	var acc = document.getElementsByClassName("accordion");
	var i;
	for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if (panel.style.display === "block") {
				panel.style.display = "none";
			} else {
				panel.style.display = "block";
			}
		});
	}
});
from .models import Personal, Cabinet, Aparat, Procedura, Pacient, Program
import time
import random

def programPacient(y, pacient_id, pac_id):
    selectii = y
    proceduri = Procedura.objects.all()
    #remove unselected procedures from selections
    details = []
    for procedura in proceduri:
        firstIndex = selectii.find(procedura.nume)
        lastIndex = selectii.rfind(procedura.nume)
        if firstIndex != -1 and firstIndex == lastIndex:
            timeString = procedura.nume + ":ztimp"
            restOfString = selectii[firstIndex:].find("||")
            if selectii.startswith(timeString,firstIndex):
                if restOfString == -1:
                    selectii = selectii[:firstIndex]
                else:
                    actualIndex = firstIndex + restOfString + 2
                    selectii = selectii[:firstIndex]+selectii[actualIndex:]
    lista_selectii = selectii.split('||')
    nume = lista_selectii[0][5:]
    if lista_selectii[1][10:] == "true":
        ambulator = True
    else:
        ambulator = False
    pacient_nou = Pacient.objects.create(pacient_id=pacient_id,pac_id=pac_id,nume=nume,ambulator=ambulator,selectii=y)
    newList = []
    #add priorities to selected procedures
    for selectie in lista_selectii[2:]:
        if selectie == "":
            continue
        info = selectie.split(':')
        print(info)
        procedura = Procedura.objects.get(nume=info[0])
        prioritate = procedura.prioritate
        selectie = str(prioritate) + ":" + selectie
        newList.append(selectie)
    #sort procedures by priority
    lista_selectii = sorted(newList, reverse=True)
    #print(lista_selectii)
    prevproc = ""
    total = 0
    count = 0
    for selectie in lista_selectii:
        info = selectie.split(':')
        procedura = Procedura.objects.get(nume=info[1])
        if prevproc != procedura:
            #for new procedure
            if count > 0:
                #save previous procedure unless it's the 1st iteration
                newtotal = getTotal4SelectedProc(prevproc,count,prevtime)
                total = total + newtotal
                if prevproc.prioritate != procedura.prioritate:
                    #find available time only if procedures have different priorities
                    #otherwise keep adding to the total time
                    details.append(checkForAvailableSpot(pacient_nou, prevproc.aparat, total))
                    total = 0
                    count = 0
            prevproc = procedura
            #print("new procedure:"+selectie)
            if len(info) == 2:
                #neither region, nor time
                count = 1
                prevtime = int(procedura.timpi)
                continue
            if len(info) == 3:
                #only regions
                count = 1
                prevtime = int(procedura.timpi)
                continue
            if len(info) == 4:
                #time selected by user
                count = 0
                prevtime = int(info[3])
                continue
        else:
            #print(selectie)
            if len(info) == 2:
                #only time, no regions
                count = 1
                continue
            if len(info) == 3:
                #add a region
                count = count + 1
                continue
    if count > 0:
        newtotal = getTotal4SelectedProc(prevproc,count,prevtime)
        total = total + newtotal
        details.append(checkForAvailableSpot(pacient_nou, prevproc.aparat, total))
    print(sorted(details))
    return sorted(details)

def getTotal4SelectedProc(procedura, regiuni, minute):
    aparat = procedura.aparat
    ref = aparat.max_regiuni
    total = (regiuni//ref + regiuni%ref)*minute
    #print("adaug:"+str(total))
    return total

def checkForAvailableSpot(pacient, aparat, total):
    full = total + aparat.pregatire_pacient
    #print("total cu pregatire "+aparat.cabinet.nume+":"+str(full))
    #get interval for type of patient
    if pacient.ambulator:
        program_start = 1200
        program_stop = 1400
    else:
        program_start = 700
        program_stop = 1200
    #get start time for last booked interval for patient if it exists
    if pacient.intervale_ocupate == "-":
        lastendtime = program_start
    else:
        intervale = pacient.intervale_ocupate.split('-')
        #add 5 for deplasare
        lastendtime = int(intervale[len(intervale)-2].split(':')[1]) + 5
    tura = 0
    intervals = []
    while tura < 2:
        #get all slots for cabinet
        slots = Program.objects.filter(cabinet=aparat.cabinet)
        for slot in slots:
            if int(slot.ora) >= lastendtime and slot.locuri > 0 and int(slot.ora) < program_stop:
                sfarsit_interval = int(getFinishTime(slot.ora,full))
                if sfarsit_interval>program_stop:
                    #continue if program goes overboard
                    continue
                else:
                    #check if patient is free in that interval
                    intervale_pacient = pacient.intervale_ocupate.split('-')
                    busy = False
                    for interval_pacient in intervale_pacient:
                        if len(interval_pacient) == 0:
                            continue
                        inceput = int(interval_pacient.split(':')[0])
                        sfarsit = int(interval_pacient.split(':')[1])
                        if int(slot.ora) < inceput and sfarsit_interval > sfarsit:
                            #print("interval encompasses another busy interval:"+interval_pacient)
                            busy = True
                            break
                        if int(slot.ora) >= inceput and int(slot.ora) <= sfarsit:
                            #print("start:"+slot.ora+";stop:"+str(sfarsit_interval))
                            #print("either start/stop is within a busy interval:"+interval_pacient)
                            busy = True
                            break
                        if sfarsit_interval >= inceput and sfarsit_interval <= sfarsit:
                            #print("start:"+slot.ora+";stop:"+str(sfarsit_interval))
                            #print("either start/stop is within a busy interval:"+interval_pacient)
                            busy = True
                            break
                        #print("start:"+slot.ora+";stop:"+str(sfarsit_interval))
                        #print("no problem??:"+interval_pacient)
                    if busy:
                        continue
                    #check if all interval is free for cabinet
                    intervals = []
                    for secondslot in slots:
                        ora = int(secondslot.ora)
                        if ora >= int(slot.ora) and ora < sfarsit_interval:
                            if secondslot.locuri > 0:
                                if aparat.nume == "Calmostim":
                                    #one more condition for calmostim bc only one person at a time (should generalize in the future)
                                    pacienti_in_paralel = secondslot.pacienti.split('-')
                                    #check if any of them also has calmostim at the same time
                                    busyAparat = False
                                    for pacient_paralel in pacienti_in_paralel:
                                        if len(pacient_paralel) == 0:
                                            continue
                                        actualpacient = Pacient.objects.get(pacient_id=pacient_paralel)
                                        if actualpacient.aparate.find("Camera 6") == actualpacient.aparate.rfind("Camera 6"):
                                            if "Camera 6" in actualpacient.aparate:
                                                busyAparat = True
                                                break
                                    if busyAparat:
                                        intervals = []
                                        break
                                #print("good one "+str(secondslot))
                                intervals.append(secondslot)
                            else:
                                intervals = []
                                break
                    if len(intervals) > 0 :
                        tura = 2
                        for interval in intervals:
                            interval.locuri = interval.locuri - 1
                            interval.pacienti = interval.pacienti + str(pacient.pacient_id) + "-"
                            interval.save()
                        pacient.intervale_ocupate = pacient.intervale_ocupate + slot.ora + ":" + str(sfarsit_interval) + "-"
                        pacient.aparate = pacient.aparate + aparat.cabinet.nume + "-"
                        pacient.save()
                        if len(slot.ora) == 3:
                            newora = "0" + slot.ora
                        else:
                            newora = slot.ora
                        return newora+":"+str(sfarsit_interval)+":"+aparat.cabinet.nume+":"+aparat.nume+":"+str(full)
                        break
        #look again from the beginning
        lastendtime = program_start
        tura = tura + 1
        if tura == 2 and program_stop != 1400:
            #go another round if internat and couldn't find spot, look for ambulator spots instead
            tura = 1
            lastendtime = 1200
            program_stop = 1400
    if len(intervals) == 0 :
        return "could not save:"+aparat.cabinet.nume+":"+aparat.nume+":"+str(full)


def getFinishTime(startTime, total):
    if len(startTime) == 3:
        intOra = int(startTime[:1])*60 + int(startTime[1:]) + total
    else:
        intOra = int(startTime[:2])*60 + int(startTime[2:]) + total
    hh = str(intOra//60)
    min = intOra % 60
    if min > 9:
        return hh+str(min)
    else:
        return hh+"0"+str(min)

def remove(pacient):
    om = Pacient.objects.get(pacient_id=pacient)
    program_pacient = Program.objects.filter(pacienti__contains=str(pacient))
    nume = str(pacient) + "-"
    for moment in program_pacient:
        moment.locuri += 1
        pacienti_moment = moment.pacienti
        moment.pacienti = pacienti_moment.replace(nume,'',1)
        moment.save()
    om.activ = False
    om.save()
    return om